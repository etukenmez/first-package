<?php
Route::group(['middleware' => ['web']], function () {
    Route::get('simple-log',[
        'as'=>'logList',
        'uses'=>'Etukenmez\SimpleLog\SimpleLOgController@index',
        'middleware' => 'auth'
    ]);
});