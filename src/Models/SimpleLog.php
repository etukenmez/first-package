<?php

namespace Etukenmez\SimpleLog\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class SimpleLog extends Model
{
    protected $fillable = [
        'user_id', 'loggable_id', 'loggable_type',
        'action', 'description', 'details', 'action_time',
        ];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function loggable()
    {
        return $this->morphTo();
    }
}
