<?php

namespace Etukenmez\SimpleLog;

use Illuminate\Support\ServiceProvider;

class SimpleLogServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__ . '/routes.php';
        $this->publishes([
            __DIR__ . '/migrations' => database_path('migrations')
        ],'migrations');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Etukenmez\SimpleLog\SimpleLogController');
        $this->loadViewsFrom(__DIR__.'/views','simplelog');
    }
}
