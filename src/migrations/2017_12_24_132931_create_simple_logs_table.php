<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimpleLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('simple_logs', function (Blueprint $table) {
            $table->id('id');
            $table->string('user_id');
            $table->integer('loggable_id');
            $table->string('loggable_type');
            $table->string('action');
            $table->string('description');
            $table->text('details');
            $table->timestamp('action_time')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('simple_logs');
    }
}
