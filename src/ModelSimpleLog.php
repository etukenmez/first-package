<?php

namespace Etukenmez\SimpleLog;

use \Illuminate\Support\Facades\Auth;
use Etukenmez\SimpleLog\Models\SimpleLog;
use Illuminate\Database\Eloquent\Model;

trait ModelSimpleLog
{

    protected static function bootModelSimpleLog(){

        foreach (static::getRecordActivityEvents() as $eventName) {

            static::$eventName(function (Model $model) use ($eventName) {
                try {
                    $reflect = new \ReflectionClass($model);
                    return SimpleLog::create([
                        'user_id'     => Auth::check() ? Auth::user()->id : \request()->ip(),
                        'loggable_id'   => $model->id,
                        'loggable_type' => get_class($model),
                        'action'      => static::getActionName($eventName),
                        'description' => ucfirst($eventName) . " a " . $reflect->getShortName(),
                        'details'     => json_encode($model->getDirty())
                    ]);
                } catch (\Exception $e) {
                    throw new \Exception($e->getMessage());
                    return new SimpleLog();
                }
            });
        }
    }

    protected static function getRecordActivityEvents()
    {
        if (isset(static::$recordEvents)) {
            return static::$recordEvents;
        }

        return [
            'created',
            'updated',
            'deleted',
        ];
    }

    /**
     * Return Suitable action name for Supplied Event
     *
     * @param $event
     * @return string
     */
    protected static function getActionName($event)
    {
        switch (strtolower($event)) {
            case 'created':
                return 'create';
                break;
            case 'updated':
                return 'update';
                break;
            case 'deleted':
                return 'delete';
                break;
            default:
                return 'unknown';
        }
    }

    protected function logs()
    {
        return $this->morphMany(SimpleLog::class, 'loggable')->orderBy('action_time', 'desc');
    }
}
