<?php

namespace Etukenmez\SimpleLog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Etukenmez\SimpleLog\Models\SimpleLog;

class SimpleLogController extends Controller
{

    public function index(){
        $logs = SimpleLog::paginate(15);

        return view('simplelog::lists',compact('logs'));
    }
}
